let filterInput = document.querySelector('#filterInput');

filterInput.addEventListener('keyup',filterNames);

function filterNames(){
    //get value of input
    let filterValue = document.querySelector('#filterInput').value .toUpperCase();
    
    //get my contacts name 
    let ul = document.querySelector('#names')
    //get Li from Ul
    let li = ul.querySelectorAll('.collection-item')

    for(i=0; i<li.length;i++){
        //get all TagNames of 'a' from loop and we want this first item to see
        let a = li[i].querySelectorAll('a')[0];
       //if matched  (innerHTML grab whatever is in 'a' tag)
       //indexOf(filterValue)> -1 ~ means that theres is match one of the  letters in the names (wrote in input)
       if(a.innerHTML.toUpperCase().indexOf(filterValue) > -1){
       //if this match
        li[i].style.display = '';
       }else{
        li[i].style.display = 'none';
       }
   
    }

}